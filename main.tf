provider "google" {
 version = "~> 3.25"
 credentials = file("panda.json")
 project = "terr-280112"
 region = "us-central1"
 zone = "us-central1-c"
}
resource "google_compute_network" "vpc_network" {
 name = "terraform-network"
}

terraform {
 backend "gcs" {
 bucket = "tf-state-prod-panda"
 prefix = "terraform/state"
 credentials = "panda.json"
 }
}